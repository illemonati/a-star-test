use std::cell::RefCell;
use std::cmp::Ordering;
use std::rc::Rc;

#[derive(Debug, Clone)]
pub struct Node {
    pub identifier: String,
    pub visited: bool,
    // Distance from start
    pub g_val: usize,
    // Heuristic distance to end
    pub h_val: usize,
    pub edges: Vec<Edge>,
    pub previous: Option<Rc<RefCell<Node>>>,
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.identifier == other.identifier
    }
}
impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.get_f_val().cmp(&other.get_f_val()))
    }
}

impl Node {
    pub fn new(identifier: impl Into<String>) -> Self {
        Node {
            identifier: identifier.into(),
            visited: false,
            g_val: std::usize::MAX,
            h_val: std::usize::MAX,
            edges: Vec::new(),
            previous: None,
        }
    }
    pub fn new_rc(identifier: impl Into<String>) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self::new(identifier)))
    }
    // Total expected distance
    pub fn get_f_val(&self) -> usize {
        let f_val = self.g_val.checked_add(self.h_val);
        match f_val {
            Some(v) => v,
            None => std::usize::MAX,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Edge {
    pub cost: usize,
    pub nodes_connected: [Rc<RefCell<Node>>; 2],
}

impl Edge {
    pub fn new<'a>(node0: Rc<RefCell<Node>>, node1: Rc<RefCell<Node>>, cost: usize) -> Self {
        Self {
            cost,
            nodes_connected: [node0, node1],
        }
    }
}

#[derive(Debug, Clone)]
pub struct Graph {
    pub nodes: Vec<Rc<RefCell<Node>>>,
}

impl Graph {
    pub fn new(nodes: Vec<Rc<RefCell<Node>>>) -> Self {
        Self { nodes }
    }
}
