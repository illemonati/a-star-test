mod test_graph;
use a_star::a_star::a_star_find_shortest;
use a_star::graph::Node;
use std::cell::RefCell;
use std::rc::Rc;

fn main() {
    let graphs = [
        test_graph::create_test_graph(),
        test_graph::create_test_graph2(),
    ];
    for graph in graphs.iter() {
        let starting_node: Rc<RefCell<Node>> = graph.nodes.get(0).expect("Get A Error").clone();
        let ending_node: Rc<RefCell<Node>> = graph
            .nodes
            .get(graph.nodes.len() - 1)
            .expect("get Z Error")
            .clone();
        let result =
            a_star_find_shortest(starting_node, ending_node, true).expect("No Paths Possible");
        println!("{}", result);
    }
}
