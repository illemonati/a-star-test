use crate::graph::Node;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;
use std::{error::Error, fmt};

#[derive(Debug)]
pub struct NoPathPossibleError;

impl Error for NoPathPossibleError {}

impl fmt::Display for NoPathPossibleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "No Path Possible")
    }
}

pub struct AStarResult {
    path: Vec<Rc<RefCell<Node>>>,
}

impl fmt::Display for AStarResult {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut costs = Vec::with_capacity(self.path.len() - 1);
        for i in 0..self.path.len() - 1 {
            let first = self.path[i].clone();
            let second = self.path[i + 1].clone();
            let node1 = first.borrow();
            let node2 = second.borrow();
            let cost = node2.g_val - node1.g_val;
            costs.push(cost);
        }
        write!(f, "Path: ")?;
        for (i, node) in self.path[..&self.path.len() - 1].iter().enumerate() {
            write!(f, "{} --{}-- ", node.borrow().identifier, costs[i])?;
        }
        writeln!(f, "{}", self.path[self.path.len() - 1].borrow().identifier)?;
        write!(
            f,
            "Total Cost: {}",
            self.path[self.path.len() - 1].borrow().g_val
        )
    }
}

pub fn a_star_find_shortest(
    starting_node: Rc<RefCell<Node>>,
    ending_node: Rc<RefCell<Node>>,
    debug: bool,
) -> Result<AStarResult, NoPathPossibleError> {
    starting_node.borrow_mut().g_val = 0;
    let mut open_list = VecDeque::new();
    open_list.push_back(starting_node);
    while !open_list.is_empty() {
        let current = open_list[0].clone();
        open_list.pop_front();
        if debug {
            println!(
                "current: {}, {:?}",
                current.borrow().identifier,
                open_list
                    .iter()
                    .map(|node| node.borrow().identifier.clone())
                    .collect::<Vec<String>>()
            );
        }
        current.borrow_mut().visited = true;
        let mut unvisited_neightbors = vec![];
        for edge in current.borrow().edges.clone() {
            let mut other_node: Option<Rc<RefCell<Node>>> = None;
            for node in edge.nodes_connected.iter() {
                if *node.borrow() != *current.borrow() {
                    other_node = Some(node.clone());
                }
            }
            let other_node = other_node.unwrap();
            let new_g_value = current
                .borrow()
                .g_val
                .checked_add(edge.cost)
                .unwrap_or(std::usize::MAX);
            if other_node.borrow().g_val > new_g_value {
                other_node.borrow_mut().g_val = new_g_value;
                other_node.borrow_mut().previous = Some(current.clone());
            }
            if other_node.borrow().visited {
                continue;
            }
            unvisited_neightbors.push(other_node);
        }

        if *current.borrow() == *ending_node.borrow() {
            let mut new_open_list: Vec<Rc<RefCell<Node>>> = open_list
                .iter()
                .filter(|node| node.borrow().g_val < ending_node.borrow().g_val)
                .cloned()
                .collect();
            new_open_list.sort_by(|a, b| a.borrow().partial_cmp(&b.borrow()).unwrap());
            new_open_list.dedup();
            open_list = VecDeque::from(new_open_list);
            // println!(
            //     "{:?}",
            //     open_list
            //         .iter()
            //         .map(|node| node.borrow().identifier.clone())
            //         .collect::<Vec<String>>()
            // );
            continue;
        }
        unvisited_neightbors.sort_by(|a, b| a.borrow().partial_cmp(&b.borrow()).unwrap());
        unvisited_neightbors.dedup();
        open_list.append(&mut VecDeque::from(unvisited_neightbors));
        let mut open_list_vec = Vec::from(open_list);
        open_list_vec.sort_by(|a, b| a.borrow().partial_cmp(&b.borrow()).unwrap());
        open_list_vec.dedup();
        open_list = VecDeque::from(open_list_vec);
    }
    let mut path: Vec<Rc<RefCell<Node>>> = vec![];
    let mut previous = Some(ending_node.clone());
    while (&previous).is_some() {
        let previous1 = previous.unwrap();
        let previous_node = previous1.borrow();
        path.push(previous1.clone());
        previous = previous_node.previous.clone();
    }
    path.reverse();
    return Ok(AStarResult { path });
}
