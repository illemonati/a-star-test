use a_star::graph::{Edge, Graph, Node};

pub fn create_test_graph() -> Graph {
    let a = Node::new_rc("A");
    a.clone().borrow_mut().h_val = 14;
    let b = Node::new_rc("B");
    b.clone().borrow_mut().h_val = 12;
    let c = Node::new_rc("C");
    c.clone().borrow_mut().h_val = 11;
    let d = Node::new_rc("D");
    d.clone().borrow_mut().h_val = 6;
    let e = Node::new_rc("E");
    e.clone().borrow_mut().h_val = 4;
    let f = Node::new_rc("F");
    f.clone().borrow_mut().h_val = 11;
    let z = Node::new_rc("Z");
    z.clone().borrow_mut().h_val = 0;
    let ab_edge = Edge::new(a.clone(), b.clone(), 4);
    let ac_edge = Edge::new(a.clone(), c.clone(), 3);
    let bf_edge = Edge::new(b.clone(), f.clone(), 5);
    let be_edge = Edge::new(b.clone(), e.clone(), 12);
    let cd_edge = Edge::new(c.clone(), d.clone(), 7);
    let ce_edge = Edge::new(c.clone(), e.clone(), 10);
    let de_edge = Edge::new(d.clone(), e.clone(), 2);
    let fz_edge = Edge::new(f.clone(), z.clone(), 16);
    let ez_edge = Edge::new(e.clone(), z.clone(), 5);
    a.clone().borrow_mut().edges = vec![ab_edge.clone(), ac_edge.clone()];
    b.clone().borrow_mut().edges = vec![ab_edge.clone(), bf_edge.clone(), be_edge.clone()];
    c.clone().borrow_mut().edges = vec![ac_edge.clone(), ce_edge.clone(), cd_edge.clone()];
    d.clone().borrow_mut().edges = vec![cd_edge.clone(), de_edge.clone()];
    e.clone().borrow_mut().edges = vec![
        be_edge.clone(),
        ce_edge.clone(),
        de_edge.clone(),
        ez_edge.clone(),
    ];
    f.clone().borrow_mut().edges = vec![bf_edge.clone(), fz_edge.clone()];
    z.clone().borrow_mut().edges = vec![fz_edge.clone(), ez_edge.clone()];
    Graph::new(vec![
        a.clone(),
        b.clone(),
        c.clone(),
        d.clone(),
        e.clone(),
        f.clone(),
        z,
    ])
}
