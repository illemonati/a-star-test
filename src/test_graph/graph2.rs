use a_star::graph::{Edge, Graph, Node};

pub fn create_test_graph2() -> Graph {
    let zero = Node::new_rc("0");
    let one = Node::new_rc("1");
    let two = Node::new_rc("2");
    let three = Node::new_rc("3");
    let four = Node::new_rc("4");
    let five = Node::new_rc("5");
    let six = Node::new_rc("6");
    let seven = Node::new_rc("7");
    zero.clone().borrow_mut().h_val = 722 / 40;
    one.clone().borrow_mut().h_val = 551 / 40;
    two.clone().borrow_mut().h_val = 523 / 40;
    three.clone().borrow_mut().h_val = 559 / 40;
    four.clone().borrow_mut().h_val = 319 / 40;
    five.clone().borrow_mut().h_val = 326 / 40;
    let zero_one_edge = Edge::new(zero.clone(), one.clone(), 1);
    let zero_two_edge = Edge::new(zero.clone(), two.clone(), 2);
    let zero_three_edge = Edge::new(zero.clone(), three.clone(), 5);
    let one_four_edge = Edge::new(one.clone(), four.clone(), 4);
    let one_five_edge = Edge::new(one.clone(), five.clone(), 11);
    let two_four_edge = Edge::new(two.clone(), four.clone(), 9);
    let two_five_edge = Edge::new(two.clone(), five.clone(), 5);
    let two_six_edge = Edge::new(two.clone(), six.clone(), 16);
    let three_six_edge = Edge::new(three.clone(), six.clone(), 2);
    let four_seven_edge = Edge::new(four.clone(), seven.clone(), 18);
    let five_seven_edge = Edge::new(five.clone(), seven.clone(), 13);
    let six_seven_edge = Edge::new(six.clone(), seven.clone(), 2);
    zero.clone().borrow_mut().edges = vec![
        zero_one_edge.clone(),
        zero_two_edge.clone(),
        zero_three_edge.clone(),
    ];
    one.clone().borrow_mut().edges = vec![
        zero_one_edge.clone(),
        one_five_edge.clone(),
        one_four_edge.clone(),
    ];
    two.clone().borrow_mut().edges = vec![
        zero_two_edge.clone(),
        two_five_edge.clone(),
        two_four_edge.clone(),
        two_six_edge.clone(),
    ];
    three.clone().borrow_mut().edges = vec![zero_three_edge.clone(), three_six_edge.clone()];
    four.clone().borrow_mut().edges = vec![
        one_four_edge.clone(),
        two_four_edge.clone(),
        four_seven_edge.clone(),
    ];
    five.clone().borrow_mut().edges = vec![
        one_five_edge.clone(),
        two_five_edge.clone(),
        five_seven_edge.clone(),
    ];
    six.clone().borrow_mut().edges = vec![
        two_six_edge.clone(),
        three_six_edge.clone(),
        six_seven_edge.clone(),
    ];
    seven.clone().borrow_mut().edges = vec![
        four_seven_edge.clone(),
        five_seven_edge.clone(),
        six_seven_edge.clone(),
    ];
    Graph {
        nodes: vec![zero, one, two, three, four, five, six, seven],
    }
}
